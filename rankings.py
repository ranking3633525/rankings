# RANKING #

from itertools import permutations as nPr

def greater(m1: list, m2: list) -> bool:
    return all([a > b for a, b in zip(m1, m2)])

def lesser(m1: list, m2: list) -> bool:
    return all(a < b for a, b in zip(m1, m2))

def rank_list(scores: list) -> list:
    ranks = []
    for name1, marks1 in scores:
        temp = []
        for name2, marks2 in scores:
            if greater(marks1, marks2) == 1 and  lesser(marks2, marks1) == 1:
                if name1 not in temp:
                    temp.append(name1)
                if name2 not in temp:
                    temp.append(name2)
        if temp != [] :
            ranks.append(temp)
    return ranks
   
def check_in(s1: str, s2: str) -> bool:
    return any(''.join(subs) in seq for seq in s2 for subs in nPr(s1, len(s1) - 1))  
                    
def format_rank_list(rank: list) -> list:
    ans = []
    rank.sort(key  = lambda x: -len(x))
    for r in rank:
        if not check_in('>'.join(r), ans):
            ans.append('>'.join(r))
    return '\n'.join(ans)


def format_input(file_name: str) -> list:
    data = open(file_name).read().replace("  "," ")
    return [[student[0],list(map(int,student[2:].split(" ")))] for student in data.strip().split("\n")]

        
# def format_output(ranks: list) -> str:    
#     return '\n'.join(format_rank_list(rank_list(ranks)))

print(format_rank_list(rank_list(format_input("student_ranking.txt"))))
